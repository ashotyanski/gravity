package com.niazarak.gravity.util;

import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool;

import static com.niazarak.gravity.GravityGame.logger;

public class VectorUtils {
    private static Pool<Vector2> vectorPool = new Pool<Vector2>() {
        @Override
        protected Vector2 newObject() {
            return new Vector2();
        }

        @Override
        protected void reset(Vector2 object) {
            object.setZero();
        }
    };

    public static Vector2 get() {
        return vectorPool.obtain();
    }

    public static Polygon makePolygon(Actor actor) {
        Vector2 v1 = actor.localToStageCoordinates(vectorPool.obtain().set(0, actor.getHeight()));
        Vector2 v2 = actor.localToStageCoordinates(vectorPool.obtain().set(actor.getWidth(), actor.getHeight()));
        Vector2 v3 = actor.localToStageCoordinates(vectorPool.obtain().set(0, 0));
        Vector2 v4 = actor.localToStageCoordinates(vectorPool.obtain().set(actor.getWidth(), 0));

        Polygon polygon = new Polygon(new float[]{
                v1.x, v1.y,
                v2.x, v2.y,
                v3.x, v3.y,
                v4.x, v4.y,
        });

        freeAll(v1, v2, v3, v4);
        return polygon;
    }


    private static void freeAll(Vector2... vectors) {
        for (int i = 0; i < vectors.length; i++) {
            vectorPool.free(vectors[i]);
        }
    }
}
