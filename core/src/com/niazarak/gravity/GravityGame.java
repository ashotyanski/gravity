package com.niazarak.gravity;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Logger;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;
import com.niazarak.gravity.base.SceneManager;

import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class GravityGame extends Game {
    public static final int WIDTH = 270;
    public static final int HEIGHT = 500;
    public static Logger logger = new Logger("????");

    private SceneManager manager;

    private static GravityGame game;

    private GravityGame() {
    }

    public static GravityGame get() {
        if (game == null) {
            game = new GravityGame();
        }
        return game;
    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        logger.setLevel(3);

        Resources.initResources();
        Resources.assetManager().load(SKIN, Skin.class);
        Resources.assetManager().finishLoading();

        Graphics.initGui();

//        Gdx.graphics.setWindowedMode(WIDTH, HEIGHT);
        manager = GravityInjector.sceneManager();
//        manager.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

        setScreen(new GravityScreen());
    }

    private void loadResources() {
    }
}
