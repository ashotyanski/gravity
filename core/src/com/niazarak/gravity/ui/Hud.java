package com.niazarak.gravity.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;
import com.niazarak.gravity.models.Score;
import com.niazarak.gravity.ui.menu.PausePlayButton;

import static com.niazarak.gravity.GravityGame.logger;
import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class Hud extends Group {
    private PausePlayButton pauseButton = new PausePlayButton(false);
    private Label scoreLabel = new Label("0", Resources.skin(SKIN), "score");

    public Hud() {
        pauseButton.setSize(Graphics.dp(30), Graphics.dp(30));
        pauseButton.setPosition(
                Gdx.graphics.getWidth() - pauseButton.getWidth() - Graphics.dp(5),
                Gdx.graphics.getHeight() - pauseButton.getHeight() - Graphics.dp(5)
        );
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                logger.debug("Pause click");
//                pauseButton.toggleUi();
                boolean isPause = pauseButton.isPause();
//                scoreLabel.setVisible(isPause);
            }
        });
        addActor(pauseButton);

        scoreLabel.setFontScale(Graphics.getPixelScale());
        scoreLabel.setPosition(0 + Graphics.dp(10), Gdx.graphics.getHeight() - scoreLabel.getHeight() - Graphics.dp(10));
        addActor(scoreLabel);
    }

    public void setOnPauseListener(final EventListener listener) {
        pauseButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                listener.handle(event);
            }
        });
    }

    public PausePlayButton getPauseButton() {
        return pauseButton;
    }

    public void applyScore(Score score) {
        scoreLabel.setText(String.valueOf(score.getScore()));
    }
}
