package com.niazarak.gravity.ui.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class Menu extends Group {
    private TextButton backButton = new MenuButton("back");
    private TextButton retryButton = new MenuButton("retry");

    public Menu() {
        retryButton.setPosition(Gdx.graphics.getWidth() / 2 - backButton.getWidth() / 2,
                Gdx.graphics.getHeight() / 5 * 2);
        addActor(retryButton);
        backButton.setPosition(Gdx.graphics.getWidth() / 2 - backButton.getWidth() / 2,
                Gdx.graphics.getHeight() / 5 * 1);
        addActor(backButton);
    }

    public void setBackButtonListener(final EventListener listener) {
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                listener.handle(event);
            }
        });
    }

    public void setRetryButtonListener(final EventListener listener) {
        retryButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                listener.handle(event);
            }
        });
    }
}
