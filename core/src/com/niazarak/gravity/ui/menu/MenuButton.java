package com.niazarak.gravity.ui.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;

import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class MenuButton extends TextButton {

    public MenuButton(String text) {
        super(text, Resources.assetManager().get(SKIN, Skin.class), "menu");
        setSize(Gdx.graphics.getWidth() / 5 * 3, Graphics.dp(40));
        setName(text);
        getLabel().setFontScale(0.6f * Graphics.getPixelScale());
    }
}
