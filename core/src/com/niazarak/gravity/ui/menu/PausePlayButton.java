package com.niazarak.gravity.ui.menu;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.niazarak.gravity.base.Resources;

import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class PausePlayButton extends Button {
    private boolean isPause;

    public PausePlayButton(boolean isPause) {
        super(Resources.assetManager().get(SKIN, Skin.class), "pause");
        this.isPause = isPause;
        setIsPause(isPause);
        setScale(2);
    }

    public void setIsPause(boolean isPause) {
        this.isPause = isPause;
        setStyle(getSkin().get(isPause ? "pause" : "play", ButtonStyle.class));
    }

    public boolean isPause() {
        return isPause;
    }
}
