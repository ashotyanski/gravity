package com.niazarak.gravity;

import com.niazarak.gravity.base.BaseInjector;
import com.niazarak.gravity.base.SceneManager;

public class GravityInjector extends BaseInjector {
    public static SceneManager sceneManager() {
        return injector().getInstance(SceneManager.class);
    }
}
