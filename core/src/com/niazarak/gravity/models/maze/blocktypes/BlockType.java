package com.niazarak.gravity.models.maze.blocktypes;


/**
 * Interface for different block types.
 */
public interface BlockType {

    /**
     * Specifies next block location for current block type.
     * Next block starts when current block ends, so this is the same to where this block turns in the end.
     * <p>
     * 0 - block goes straight down
     * 1 - block goes left
     * 2 - block goes straight up
     * 3 - block goes right
     * </p>
     * NB! 2 is actually never returned, because flow is oriented top-down
     */
    byte getNextBlockLocation();

    /**
     * Returns boolean-like block map for given block location, where 1 is for block tile and 0 is for empty tile
     * Block location actually defines the rotation of map.
     */
    byte[][] getBlockMap(byte blockLocation);
}
