package com.niazarak.gravity.models.maze.blocktypes;

import com.badlogic.gdx.utils.Array;

public class BlockTypes {
    public static BlockType TURN_LEFT = new TurnLeftBlock();
    public static BlockType TURN_RIGHT = new TurnRightBlock();
    public static BlockType STRAIGHT = new StraightBlock();
    public static final Array<BlockType> BLOCK_TYPES = new Array<BlockType>();

    static {
        BLOCK_TYPES.add(TURN_LEFT);
        BLOCK_TYPES.add(TURN_RIGHT);
        BLOCK_TYPES.add(STRAIGHT);
    }

    public static BlockType getRandomType() {
        return BLOCK_TYPES.random();
//        return BLOCK_TYPES.get(1);
    }
}
