package com.niazarak.gravity.models.maze.blocktypes;

public class TurnRightBlock extends TurnBlock {
    @Override
    public byte getNextBlockLocation() {
        return 3;
    }

    @Override
    public byte[][] getBlockMap(byte blockLocation) {
        switch (blockLocation) {
            case 0:
                return _2;
            case 1:
                return _3;
            case 2:
                return _0;
            case 3:
                return _1;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
