package com.niazarak.gravity.models.maze.blocktypes;

public class TurnLeftBlock extends TurnBlock {
    @Override
    public byte getNextBlockLocation() {
        return 1;
    }

    @Override
    public byte[][] getBlockMap(byte blockLocation) {
        switch (blockLocation) {
            case 0:
                return _1;
            case 1:
                return _2;
            case 2:
                return _3;
            case 3:
                return _0;
            default:
                throw new IllegalArgumentException();
        }
    }

}
