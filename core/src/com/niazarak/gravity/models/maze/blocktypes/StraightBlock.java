package com.niazarak.gravity.models.maze.blocktypes;

public class StraightBlock implements BlockType {
    @Override
    public byte getNextBlockLocation() {
        return 0;
    }

    @Override
    public byte[][] getBlockMap(byte blockLocation) {
        switch (blockLocation) {
            case 0:
                return _2;
            case 2:
                return _0;
            case 1:
                return _1;
            case 3:
                return _3;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static byte[][] _0 = new byte[][]{
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1},
            {1, 1, 1, 2, 0, 0, -2, 1, 1, 1}
    };
    private static byte[][] _1 = new byte[][]{
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {4, 4, 4, 4, 4, 4, 4, 4, 4, 4},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    };
    private static byte[][] _2 = new byte[][]{
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1},
            {1, 1, 1, -2, 0, 0, 2, 1, 1, 1}
    };
    private static byte[][] _3 = new byte[][]{
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {-4, -4, -4, -4, -4, -4, -4, -4, -4, -4},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {4, 4, 4, 4, 4, 4, 4, 4, 4, 4},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
    };
}
