package com.niazarak.gravity.models.maze;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.utils.Queue;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.models.AppleCollisionDetector;
import com.niazarak.gravity.models.maze.blocktypes.BlockType;
import com.niazarak.gravity.models.maze.blocktypes.BlockTypes;
import com.niazarak.gravity.util.Pair;
import com.niazarak.gravity.util.VectorUtils;
import org.jetbrains.annotations.NotNull;

import static com.niazarak.gravity.GravityGame.logger;

public class Maze extends Group {
    @NotNull
    private Queue<MazeBlock> blocks;
    /**
     * Where next block will be placed
     */
    private byte nextBlockLocation = 2;

    private float speed = 1;

    private boolean isMoving = true;
    private boolean hasEntered = false;

    private AppleCollisionDetector detector;
    private AppleCollisionDetector generationDetector;

    public Maze() {
        blocks = new Queue<>();
        logger.debug("Maze on " + getX() + ", " + getY());

        Circle circle = new Circle(stageToLocalCoordinates(
                new Vector2(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2)), Graphics.dp(10));
        generationDetector = new AppleCollisionDetector(circle) {
            @Override
            protected void onCollided() {
                generate();
                if (!hasEntered) {
                    hasEntered = true;
                }
            }
        };
    }

    public void prepare() {
        speed = 1;
        nextBlockLocation = 2;
        hasEntered = false;
        blocks.clear();
        clearChildren();
        setRotation(0);
    }

    private void generate() {
        MazeBlock nextMazeBlock = createMazeBlock();

        // init position
        Pair<Float, Float> blockPosition;
        if (blocks.size == 0) {
            blockPosition = getPositionForFirstBlock(nextMazeBlock);
        } else {
            blockPosition = blocks.last().getNextBlockPosition(nextBlockLocation);
        }
        nextMazeBlock.setPosition(blockPosition.a, blockPosition.b);

        // add to queue
        blocks.addLast(nextMazeBlock);
        // inflate
        nextMazeBlock.inflate(nextBlockLocation);
        // update next location
        nextBlockLocation = (byte) ((nextBlockLocation + nextMazeBlock.getBlockType().getNextBlockLocation()) % 4);

        logger.debug("MazeBlock on " + nextMazeBlock.getX() + ", " + nextMazeBlock.getY());
        logger.debug("MazeBlock size " + nextMazeBlock.getWidth() + ", " + nextMazeBlock.getHeight());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isMoving) {
            if (blocks.size == 0) {
                generate();
            }

            Polygon polygon = VectorUtils.makePolygon(blocks.last());
            generationDetector.detect(polygon);
        }
    }

    public void setCollisionDetector(@NotNull AppleCollisionDetector collisionDetector) {
        this.detector = collisionDetector;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    @NotNull
    private Pair<Float, Float> getPositionForFirstBlock(MazeBlock currentBlock) {
        float x = -currentBlock.getWidth() / 2;
        float y = -currentBlock.getHeight() / 2 * 3;
        return new Pair<>(x, y);
    }

    private MazeBlock createMazeBlock() {
        final MazeBlock block;
        final BlockType blockType = BlockTypes.getRandomType();
        if (blocks.size < 3) {
            block = new MazeBlock(blockType);
            block.addAction(getGravityAction(block));
            block.setAppleCollisionDetector(detector);
            addActor(block);
            logger.debug("Block added");
        } else {
            block = blocks.removeFirst();
            block.setBlockType(blockType);
        }
        return block;
    }

    public void setIsMoving(boolean move) {
        isMoving = move;
        if (!move) {
            clearActions();
        }
    }

    private RepeatAction getGravityAction(final MazeBlock block) {
        return Actions.repeat(-1, Actions.run(new Runnable() {
            @Override
            public void run() {
                if (isMoving) {
                    float radians = -(getRotation() + 90) / 180.0f * MathUtils.PI;
                    float cos = -MathUtils.cos(radians);
                    float sin = -MathUtils.sin(radians);
                    block.moveBy(Graphics.dp(cos) * speed, Graphics.dp(sin) * speed);
                }
            }
        }));
    }

    public boolean isMoving() {
        return isMoving;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public boolean hasEntered() {
        return hasEntered;
    }
}
