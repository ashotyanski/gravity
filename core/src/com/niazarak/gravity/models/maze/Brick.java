package com.niazarak.gravity.models.maze;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Pool;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;
import com.niazarak.gravity.models.AppleCollisionDetector;
import com.niazarak.gravity.util.VectorUtils;
import org.jetbrains.annotations.Nullable;

import static com.niazarak.gravity.GravityGame.logger;
import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class Brick extends Actor implements Pool.Poolable {
    private static float brickSize = Graphics.dp(32);
    private Sprite rock;
    private byte type = Type.BASE;

    @Nullable
    private AppleCollisionDetector collisionDetector;

    public Brick(byte type) {
//        TextureRegion rockTexture = Resources.skin(SKIN).get("ground", TextureRegion.class);
//        rock = new Sprite(new TextureRegion(rockTexture, 160, 160, 32, 32));
        this.type = type;
        rock = getSpriteForType(type);
//        } else {
//            rock = new Sprite(new TextureRegion(rockTexture, 0, 0, 32, 32));
//        }

        setSize(brickSize, brickSize);
        rock.setSize(getWidth(), getHeight());
    }

    public void setCollisionDetector(@Nullable AppleCollisionDetector detector) {
        this.collisionDetector = detector;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (collisionDetector != null && isCollidable(type)) {
            Polygon polygon = VectorUtils.makePolygon(this);
            if (collisionDetector.detect(polygon)) {
                logger.debug("Collision in " + localToStageCoordinates(Vector2.Zero));
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(rock, getX(), getY(), getOriginX(), getOriginY(),
                getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
    }

    @Override
    public void reset() {
        collisionDetector = null;
    }

    public void setType(byte type) {
        this.type = type;
        rock = getSpriteForType(type);
    }

    private static Sprite getSpriteForType(byte type) {
        TextureRegion rockTexture;
        rockTexture = Resources.skin(SKIN).get("ground_" + type, TextureRegion.class);

//        switch (type) {
//            case Type.TOP: {
//                rockTexture = Resources.skin(SKIN).get("ground-4", TextureRegion.class);
//                break;
//            }
//            case Type.BOTTOM: {
//                rockTexture = Resources.skin(SKIN).get("ground-4", TextureRegion.class);
//                break;
//            }
//            case Type.RIGHT: {
//                rockTexture = Resources.skin(SKIN).get("ground-2", TextureRegion.class);
//                break;
//            }
//            case Type.LEFT: {
//                rockTexture = Resources.skin(SKIN).get("ground-2", TextureRegion.class);
//                break;
//            }
//            case Type.TL: {
//                rockTexture = Resources.skin(SKIN).get("ground-3", TextureRegion.class);
//                break;
//            }
//            case Type.BR: {
//                rockTexture = Resources.skin(SKIN).get("ground-3", TextureRegion.class);
//                break;
//            }
//            case Type.TR: {
//                rockTexture = Resources.skin(SKIN).get("ground-3", TextureRegion.class);
//                break;
//            }
//            case Type.BL: {
//                rockTexture = Resources.skin(SKIN).get("ground-3", TextureRegion.class);
//                break;
//            }
//            case Type.BASE:
//            default: {
//                rockTexture = Resources.skin(SKIN).get("ground-1", TextureRegion.class);
//                break;
//            }
//        }
        return new Sprite(rockTexture);
    }

    private static boolean isCollidable(byte type) {
//        return type != 0 && type != 1;
        return false;
    }

    public static class Type {
        public static final byte EMPTY = 0;
        public static final byte BASE = 1;
        public static final byte TOP = 4;
        public static final byte BOTTOM = -4;
        public static final byte LEFT = 2;
        public static final byte RIGHT = -2;
        public static final byte TL = 3;
        public static final byte BR = -3;
        public static final byte TR = 5;
        public static final byte BL = -5;
    }
}
