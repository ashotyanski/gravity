package com.niazarak.gravity.models.maze;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Pool;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;
import com.niazarak.gravity.models.AppleCollisionDetector;
import com.niazarak.gravity.models.maze.blocktypes.BlockType;
import com.niazarak.gravity.util.Pair;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static com.niazarak.gravity.GravityGame.logger;
import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class MazeBlock extends Group {
    private final byte BLOCK_SIZE = 10;
    private static float blockSize = Graphics.dp(320);
    @NotNull
    private BlockType blockType;
    @NotNull
    private Brick[][] bricks = new Brick[BLOCK_SIZE][BLOCK_SIZE];
    @Nullable
    private AppleCollisionDetector detector;
    @NotNull
    private Sprite mazeBlockBackground;
//    @NotNull
//    private static MazeBlockInflater inflater = new MazeBlockInflater();

    private static Pool<Brick> brickPool = new Pool<Brick>() {
        @Override
        protected Brick newObject() {
            return new Brick(Brick.Type.BASE);
        }
    };

    public MazeBlock(@NotNull BlockType blockType) {
        this.blockType = blockType;
        setSize(blockSize, blockSize);
        TextureRegion region = Resources.skin(SKIN).get("ground_bg", TextureRegion.class);
        mazeBlockBackground = new Sprite(region);
        mazeBlockBackground.setSize(getWidth(), getHeight());
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
//        batch.draw(mazeBlockBackground, 0, 0);
        batch.draw(mazeBlockBackground, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(),
                getScaleX(), getScaleY(), getRotation());
        super.draw(batch, parentAlpha);
    }

    public void inflate(byte blockLocation) {
        byte[][] blockMap = blockType.getBlockMap(blockLocation);
        for (byte i = 0; i < BLOCK_SIZE; i++) {
            for (byte j = 0; j < BLOCK_SIZE; j++) {
                if (blockMap[j][i] != 0) {
                    if (bricks[j][i] == null) {
                        bricks[j][i] = brickPool.obtain();
                        bricks[j][i].setCollisionDetector(detector);
                        bricks[j][i].setType(blockMap[j][i]);
                        if (!bricks[j][i].hasParent()) {
                            addActor(bricks[j][i]);
                        }
                    }
                    float brickW = bricks[j][i].getWidth();
                    float brickH = bricks[j][i].getHeight();
                    bricks[j][i].setPosition(i * brickW, brickH * j);
                } else {
                    if (bricks[j][i] != null) {
                        removeActor(bricks[j][i]);
                        brickPool.free(bricks[j][i]);
                        bricks[j][i] = null;
                    }
                }
            }
        }
        logger.debug("Brick pool size is " + brickPool.peak);
    }

    @NotNull
    public BlockType getBlockType() {
        return blockType;
    }

    @SuppressWarnings("ConstantConditions")
    public void setBlockType(@NotNull BlockType blockType) {
        if (blockType == null) {
            throw new NullPointerException();
        }
        this.blockType = blockType;
    }

    @NotNull
    public Pair<Float, Float> getNextBlockPosition(byte nextBlockLocation) {
        float x = getX();
        float y = getY();
        switch (nextBlockLocation) {
            case 0: {
                y += getHeight();
                break;
            }
            case 1: {
                x += getWidth();
                break;
            }
            case 2: {
                y -= getHeight();
                break;
            }
            case 3: {
                x -= getWidth();
                break;
            }
        }
        return new Pair<Float, Float>(x, y);
    }

    public void setAppleCollisionDetector(AppleCollisionDetector collisionDetector) {
        this.detector = collisionDetector;
    }
}
