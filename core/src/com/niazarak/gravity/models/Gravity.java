package com.niazarak.gravity.models;

import com.badlogic.gdx.Gdx;
import com.niazarak.gravity.base.Graphics;

public class Gravity {
    private float counter = 0;
    private float velocity = 0;
    private float acceleration;

    public Gravity() {
        acceleration = Graphics.dp(0.2f);
    }

    public void update() {
        counter += Gdx.graphics.getDeltaTime();
    }

    public float getVelocity() {
        velocity = func();
        return velocity;
    }

    public void clear() {
        velocity = 0;
        counter = 0;
    }

    private float func() {
        return (float) (3 * Math.pow(counter, 0.2));
    }
}
