package com.niazarak.gravity.models;

public class Score {
    private float multiplier = 1;
    private float delta = 0.01f;
    private float score = 0;

    public Score(int score) {
        this.score = score;
    }

    public void updateScore() {
        score += delta;
    }

    public void clearScore() {
        score = 0;
        delta = 0.01f;
        multiplier = 1;
    }

    public int getScore() {
        return (int) score;
    }

    public float setMultiplier() {
        return multiplier;
    }
}
