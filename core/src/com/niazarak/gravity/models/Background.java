package com.niazarak.gravity.models;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.niazarak.gravity.base.Resources;

import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class Background extends Actor {
    private Sprite bgSprite;

    public Background() {
        setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        setPosition(0, 0);
        bgSprite = Resources.assetManager().get(SKIN, Skin.class).getSprite("bg");
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.draw(bgSprite, getX(), getY(), getWidth(), getHeight());
    }
}
