package com.niazarak.gravity.models;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;
import org.jetbrains.annotations.NotNull;

import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class Apple extends Actor {
    private Sprite appleSprite;
    @NotNull
    private Circle circle;

    public Apple() {
        appleSprite = new Sprite(Resources.assetManager()
                .get(SKIN, Skin.class)
                .getSprite("apple"));
        appleSprite.setSize(Graphics.dp(54), Graphics.dp(70));
        setWidth(appleSprite.getWidth());
        setHeight(appleSprite.getHeight());
        setOrigin(appleSprite.getWidth() / 2, appleSprite.getHeight() / 2);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
//        batch.draw(appleSprite, getX(), getY());
        batch.draw(appleSprite, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), 1, 1, getRotation());
    }

    @NotNull
    public Circle getCircle() {
        Vector2 appleVector = localToStageCoordinates(new Vector2(appleSprite.getWidth() / 2, appleSprite.getHeight() / 2));
        return new Circle(appleVector.x, appleVector.y, appleSprite.getWidth() / 2);
    }
}
