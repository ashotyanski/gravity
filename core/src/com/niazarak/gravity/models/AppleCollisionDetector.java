package com.niazarak.gravity.models;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

import static com.niazarak.gravity.GravityGame.logger;

public abstract class AppleCollisionDetector {
    private Circle circle;

    public AppleCollisionDetector(Circle circle) {
        this.circle = circle;
    }

    public boolean detect(Polygon brick) {
        if (overlaps(brick, circle)) {
            collision();
            return true;
        }
        return false;
    }

    public boolean detect(Circle circle) {
        if (overlaps(circle, this.circle)) {
            collision();
            return true;
        }
        return false;
    }

    private void collision() {
        logger.debug("collision");
        logger.debug(circle.toString());
        onCollided();
    }

    public static boolean overlaps(Circle circle1, Circle circle2) {
        return Intersector.overlaps(circle1, circle2);
    }

    public static boolean overlaps(Polygon polygon, Circle circle) {
        float[] vertices = polygon.getTransformedVertices();
        Vector2 center = new Vector2(circle.x, circle.y);
        float squareRadius = circle.radius * circle.radius;
        for (int i = 0; i < vertices.length; i += 2) {
            if (i == 0) {
                if (Intersector.intersectSegmentCircle(new Vector2(vertices[vertices.length - 2], vertices[vertices.length - 1]), new Vector2(vertices[i], vertices[i + 1]), center, squareRadius)) {
                    return true;
                }
            } else {
                if (Intersector.intersectSegmentCircle(new Vector2(vertices[i - 2], vertices[i - 1]), new Vector2(vertices[i], vertices[i + 1]), center, squareRadius)) {
                    return true;
                }
            }
        }
        return polygon.contains(circle.x, circle.y);
    }

    protected abstract void onCollided();
}
