package com.niazarak.gravity;

public class ResourcesConstants {
    public static final String SKIN = "skin.json";
    public static final String TEXT_STYLE_TEST = "green";
    public static final String BUTTONS_DIR = "images/buttons/";
    public static final String MENU_UP = BUTTONS_DIR + "button_bg.png";
    public static final String PLAY_UP = BUTTONS_DIR + "play_up.png";
    public static final String PLAY_DOWN = BUTTONS_DIR + "play_down.png";
    public static final String PAUSE_UP = BUTTONS_DIR + "pause_up.png";
    public static final String PAUSE_DOWN = BUTTONS_DIR + "pause_down.png";

}
