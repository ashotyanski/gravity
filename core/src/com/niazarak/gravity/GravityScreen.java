package com.niazarak.gravity;

import com.niazarak.gravity.base.BaseScreen;
import com.niazarak.gravity.scenes.StartScene;
import com.niazarak.gravity.scenes.TestScene;

public class GravityScreen extends BaseScreen {
    @Override
    public void show() {
        sceneManager.open(new StartScene());
//        sceneManager.open(new TestScene());
    }
}
