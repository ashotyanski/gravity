package com.niazarak.gravity.base;


import com.badlogic.gdx.Screen;
import com.badlogic.gdx.input.GestureDetector;
import com.niazarak.gravity.GravityInjector;

public class BaseScreen extends GestureDetector.GestureAdapter implements Screen {
    protected SceneManager sceneManager = GravityInjector.sceneManager();

    public BaseScreen() {
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Scene currentScene = sceneManager.getCurrentScene();
        if (currentScene != null) {
            currentScene.act(delta);
            currentScene.draw();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }
}
