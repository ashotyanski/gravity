package com.niazarak.gravity.base;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.I18NBundle;

public class Resources {
    private static AssetManager assetManager;
    private static I18NBundle i18bundle;

    public static AssetManager assetManager() {
        if (assetManager == null) {
            assetManager = new AssetManager();
        }
        return assetManager;
    }

    public static void setAssetManager(AssetManager assetManager) {
        Resources.assetManager = assetManager;
    }

    public static I18NBundle i18bundle() {
        if (i18bundle == null)
            i18bundle = new I18NBundle();
        return i18bundle;
    }

    public static Skin skin(String skinName) {
        return assetManager().get(skinName, Skin.class);
    }

    public static void initResources() {
        assetManager();
        i18bundle();
    }

}
