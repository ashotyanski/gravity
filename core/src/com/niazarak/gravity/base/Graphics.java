package com.niazarak.gravity.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 * Utility class for graphic objects.
 */
public class Graphics {
    private static final int DEFAULT_DPI = 120;
    private static SpriteBatch spriteBatch;
    private static ModelBatch modelBatch;
    private static ShapeRenderer shapeRenderer;

    public static SpriteBatch spriteBatch() {
        if (spriteBatch == null) {
            spriteBatch = new SpriteBatch();
        }
        return spriteBatch;
    }

    public static void setSpriteBatch(SpriteBatch spriteBatch) {
        Graphics.spriteBatch.dispose();
        Graphics.spriteBatch = spriteBatch;
    }

    /**
     * Provides singletone model batch
     *
     * @return model batch
     */
    public static ModelBatch modelBatch() {
        if (modelBatch == null) {
            modelBatch = new ModelBatch();
        }
        return modelBatch;
    }

    public static void setModelBatch(ModelBatch modelBatch) {
        Graphics.modelBatch.dispose();
        Graphics.modelBatch = modelBatch;
    }

    /**
     * Provides singletone shape renderer
     *
     * @return shape renderer
     */
    public static ShapeRenderer shapeRenderer() {
        if (shapeRenderer == null) {
            shapeRenderer = new ShapeRenderer();
        }
        return shapeRenderer;
    }

    public static void setShapeRenderer(ShapeRenderer shapeRenderer) {
        Graphics.shapeRenderer.dispose();
        Graphics.shapeRenderer = shapeRenderer;
    }

    /**
     * Shorthand version for getting screen width
     *
     * @return width in pixels
     */
    public static float width() {
        return Gdx.graphics.getWidth();
    }

    /**
     * Shorthand version for getting screen height
     *
     * @return height in pixels
     */
    public static float height() {
        return Gdx.graphics.getHeight();
    }

    /**
     * Makes screen dependent pixels from independent ones
     *
     * @return scaled pixels
     */
    public static float dp(float px) {
        return getPixelScale() * px;
    }

    /**
     * Returns current pixel scale
     *
     * @return scale
     */
    public static float getPixelScale() {
        return Gdx.graphics.getPpiX() / DEFAULT_DPI;
    }

    public static void initGui() {
        shapeRenderer();
    }
}
