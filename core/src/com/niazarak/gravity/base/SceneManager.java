package com.niazarak.gravity.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.google.inject.Singleton;

import java.util.Stack;

@Singleton
public class SceneManager {
    private Stack<Scene> scenes;
    private Viewport viewport;

    public SceneManager() {
        viewport = new ScreenViewport();
        scenes = new Stack<>();
        OrthographicCamera camera = (OrthographicCamera) viewport.getCamera();
        camera.setToOrtho(false);
//        viewport.setCamera(camera);
        Graphics.shapeRenderer().setProjectionMatrix(camera.combined);
        Graphics.spriteBatch().setProjectionMatrix(camera.combined);
    }

    public Scene getCurrentScene() {
        if (!scenes.empty()) {
            return scenes.peek();
        }
        return null;
    }

    public void back() {
        scenes.pop();
        apply(scenes.peek());
    }

    public void open(Scene scene) {
        scenes.push(scene);
        apply(scene);
    }

    private void apply(Scene scene) {
        Gdx.input.setInputProcessor(scene.getInputProcessor());
    }

    public int pathLength() {
        return scenes.size();
    }

    public Viewport getViewport() {
        return viewport;
    }

    public void setSize(int width, int height) {
        viewport.setWorldSize(width, height);
        viewport.update(width, height, true);
    }
}
