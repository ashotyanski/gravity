package com.niazarak.gravity.base;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Base class for implementing your own injector. Simply extend this class and add your own providers.
 */
public class BaseInjector {
    private static Injector injector = null;

    public static Injector injector() {
        if (injector == null) {
            injector = Guice.createInjector();
        }
        return injector;
    }
}
