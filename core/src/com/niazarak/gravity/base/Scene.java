package com.niazarak.gravity.base;

import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.niazarak.gravity.GravityInjector;

/**
 * Base class for scene
 */
public class Scene extends Stage {

    protected InputMultiplexer inputMultiplexer;

    protected static SceneManager sceneManager = GravityInjector.sceneManager();

    public Scene() {
        this(sceneManager.getViewport(), Graphics.spriteBatch());
    }

    public Scene(SpriteBatch batch) {
        this(sceneManager.getViewport(), batch);
    }

    public Scene(Viewport viewport, SpriteBatch spriteBatch) {
        super(viewport, spriteBatch);
        spriteBatch.setProjectionMatrix(sceneManager.getViewport().getCamera().combined);
        inputMultiplexer = new InputMultiplexer(this);
    }

    public InputProcessor getInputProcessor() {
        return inputMultiplexer;
    }
}
