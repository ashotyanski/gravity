package com.niazarak.gravity.scenes;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.niazarak.gravity.base.Graphics;
import com.niazarak.gravity.base.Resources;
import com.niazarak.gravity.base.Scene;
import com.niazarak.gravity.models.Background;

import static com.niazarak.gravity.ResourcesConstants.SKIN;

public class TestScene extends Scene {
    private Background background = new Background();
    private Sprite sprite = Resources.assetManager().get(SKIN, Skin.class).getSprite("bg");

    public TestScene() {
        addActor(background);
    }
}
