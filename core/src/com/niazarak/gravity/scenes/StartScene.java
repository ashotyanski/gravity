package com.niazarak.gravity.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.niazarak.gravity.base.Scene;
import com.niazarak.gravity.models.Background;
import com.niazarak.gravity.ui.menu.MenuButton;

public class StartScene extends Scene {
    private Background background = new Background();

    private TextButton startButton = new MenuButton("Start");
    private TextButton settingsButton = new MenuButton("settings");

    public StartScene() {
        background.setPosition(Gdx.graphics.getWidth() / 2 - background.getWidth() / 2,
                Gdx.graphics.getHeight() / 2 - background.getHeight() / 2);
        addActor(background);

        startButton.setPosition(Gdx.graphics.getWidth() / 2 - startButton.getWidth() / 2,
                Gdx.graphics.getHeight() / 5 * 2);
        startButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                sceneManager.open(new GameScene());
            }
        });
        addActor(startButton);

        settingsButton.setPosition(Gdx.graphics.getWidth() / 2 - settingsButton.getWidth() / 2,
                Gdx.graphics.getHeight() / 5 * 1);
        addActor(settingsButton);
    }
}
