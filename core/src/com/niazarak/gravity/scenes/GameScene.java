package com.niazarak.gravity.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.niazarak.gravity.base.Scene;
import com.niazarak.gravity.models.*;
import com.niazarak.gravity.models.maze.Maze;
import com.niazarak.gravity.ui.Hud;
import com.niazarak.gravity.ui.menu.Menu;
import org.jetbrains.annotations.NotNull;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import static com.niazarak.gravity.GravityGame.logger;

public class GameScene extends Scene {
    private StateManager stateManager = new StateManager();

    private Score score = new Score(0);
    private Gravity gravity = new Gravity();

    private Background background = new Background();
    private Apple apple = new Apple();
    private Maze maze = new Maze();
    private AppleCollisionDetector appleCollisionDetector;

    private Hud hud = new Hud();
    private Menu menu = new Menu();

    public GameScene() {
        super();
        initBackground();
        initMaze();
        initApple();
        initMenu();
        initHud();

        appleCollisionDetector = new AppleCollisionDetector(apple.getCircle()) {
            @Override
            protected void onCollided() {
                logger.debug("yahoooo");
                fail();
            }
        };
        maze.setCollisionDetector(appleCollisionDetector);

        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                logger.debug(String.format("Stage clicked at {%s;%s}", event.getStageX(), event.getStageY()));
                super.clicked(event, x, y);
            }
        });
//        inputMultiplexer.addProcessor(new DebugInputAdapter(3));
//        setDebugAll(true);

        play();
    }

    private void initBackground() {
        background.addListener(getRotationTapListener());
        addActor(background);
    }

    private void initMaze() {
        maze.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);
        maze.setVisible(true);
        maze.setIsMoving(false);
        maze.addListener(getRotationTapListener());
        addActor(maze);
    }

    private void initMenu() {
        menu.setVisible(false);
        menu.setBackButtonListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                sceneManager.back();
                return true;
            }
        });
        menu.setRetryButtonListener(new EventListener() {
            @Override
            public boolean handle(Event e) {
                prepareGame();
                return true;
            }
        });
        addActor(menu);
    }

    private void initHud() {
        hud.setOnPauseListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                logger.debug("Pause click");
                if (stateManager.getState() == State.PLAY) {
                    pause();
                } else {
                    play();
                }
                return true;
            }
        });
        hud.applyScore(score);
        addActor(hud);
    }

    private void initApple() {
        apple.setPosition(Gdx.graphics.getWidth() / 2 - apple.getWidth() / 2,
                Gdx.graphics.getHeight() / 2 - apple.getHeight() / 2);

        final RotateByAction action = new RotateByAction();
        action.setAmount(30);
        action.setDuration(1);
        apple.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                action.reset();
                apple.addAction(rotateBy(30, 1));
            }
        });
        addActor(apple);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (stateManager.getState() == State.PLAY) {
            score.updateScore();
            hud.applyScore(score);

            gravity.update();

            maze.setSpeed(gravity.getVelocity());
        }
    }

    private void pause() {
        stateManager.setState(State.PAUSE);
        hud.getPauseButton().setIsPause(false);
        menu.setVisible(true);
        maze.setIsMoving(false);
    }

    private void play() {
        stateManager.setState(State.PLAY);
        hud.getPauseButton().setIsPause(true);
        menu.setVisible(false);
        maze.setIsMoving(true);
    }

    private void fail() {
        stateManager.setState(State.FINISHED);
        menu.setVisible(true);
        hud.getPauseButton().setVisible(false);
        maze.setIsMoving(false);
    }

    private void prepareGame() {
        gravity.clear();

        score.clearScore();
        hud.applyScore(score);
        hud.getPauseButton().setIsPause(false);
        hud.getPauseButton().setVisible(true);

        menu.setVisible(false);

        maze.prepare();
        maze.setPosition(Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() / 2);

        play();
    }

    @NotNull
    private ClickListener getRotationTapListener() {
        return new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (stateManager.getState() == State.PLAY && maze.hasEntered()) {
                    maze.addAction(getRotateAction(event.getStageX()));
                }
            }
        };
    }

    private RotateByAction getRotateAction(float x) {
//        return rotateBy(isLeftPane(x) ? 90 : -90);
        return rotateBy(isLeftPane(x) ? 90 : -90, 1 / gravity.getVelocity(), Interpolation.smooth);
    }

    private boolean isLeftPane(float x) {
        return x < Gdx.graphics.getWidth() / 2;
    }

    private class DebugInputAdapter extends InputAdapter {

        private int step;

        Action moveUpAction;
        Action moveDownAction;
        Action moveLeftAction;
        Action moveRightAction;

        DebugInputAdapter(int step) {
            this.step = step;


        }

        @Override
        public boolean keyDown(int keycode) {
            switch (keycode) {
                case Input.Keys.UP: {
                    moveUpAction = repeat(-1, moveBy(0, this.step));
                    maze.addAction(moveUpAction);
                    break;
                }
                case Input.Keys.DOWN: {
                    moveDownAction = repeat(-1, moveBy(0, -this.step));
                    maze.addAction(moveDownAction);
                    break;
                }
                case Input.Keys.LEFT: {
                    moveLeftAction = repeat(-1, moveBy(-this.step, 0));
                    maze.addAction(moveLeftAction);
                    break;
                }
                case Input.Keys.RIGHT: {
                    moveRightAction = repeat(-1, moveBy(this.step, 0));
                    maze.addAction(moveRightAction);
                    break;
                }
            }
            return true;
        }

        @Override
        public boolean keyUp(int keycode) {
            switch (keycode) {
                case Input.Keys.UP: {
                    maze.removeAction(moveUpAction);
                    break;
                }
                case Input.Keys.DOWN: {
                    maze.removeAction(moveDownAction);
                    break;
                }
                case Input.Keys.LEFT: {
                    maze.removeAction(moveLeftAction);
                    break;
                }
                case Input.Keys.RIGHT: {
                    maze.removeAction(moveRightAction);
                    break;
                }
            }
            return true;
        }
    }

    class StateManager {
        private State state = State.PAUSE;

        public void setState(State state) {
            this.state = state;
        }

        public State getState() {
            return state;
        }
    }

    enum State {
        PLAY, PAUSE, FINISHED
    }
}